import UIKit

final class ViewController: UIViewController {

  var presenter: TaskPresenter?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    presenter?.didLoad()
  }
}
