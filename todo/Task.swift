import Foundation
import CoreLocation.CLLocation

struct Task {
  let identifier: Int
  let name: String
  let dueDate: Date?
}
