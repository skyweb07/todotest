import Foundation

protocol TaskRepository {
  func getAll(_ completion: ([Task]) -> Void)
  func findWith(identifier: Int, _ completion: (Task?) -> Void)
  func add(task: Task, _ completion: () -> Void)
  func update(task: Task, _ completion: () -> Void)
  func delete(task: Task, _ completion: () -> Void)
}
