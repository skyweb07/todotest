import Foundation

struct DeleteTask {
  
  private let taskRepository: TaskRepository
  
  init(taskRepository: TaskRepository) {
    self.taskRepository = taskRepository
  }
  
  func execute(with task: Task, _ completion: (() -> Void)) {
    taskRepository.delete(task: task, completion)
  }
}

// MARK: - Init

extension DeleteTask {
  public init() {
    self.taskRepository = InMemoryTaskRepository()
  }
}
