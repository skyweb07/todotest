import Foundation

struct GetAllTasks {
  
  private let taskRepository: TaskRepository
  
  init(taskRepository: TaskRepository) {
    self.taskRepository = taskRepository
  }
  
  func execute(_ completiton: ([Task]) -> Void) {
    taskRepository.getAll(completiton)
  }
}

// MARK: - Init

extension GetAllTasks {
  public init() {
    self.taskRepository = InMemoryTaskRepository()
  }
}
