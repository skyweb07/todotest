import Foundation

struct AddTask {
  
  private let taskRepository: TaskRepository
  
  init(taskRepository: TaskRepository) {
    self.taskRepository = taskRepository
  }
  
  func execute(with task: Task, _ completion: (() -> Void)) {
    taskRepository.add(task: task, completion)
  }
}

// MARK: - Init

extension AddTask {
  public init() {
    self.taskRepository = InMemoryTaskRepository()
  }
}
