import Foundation

struct EditTask {
  
  private let taskRepository: TaskRepository
  
  init(taskRepository: TaskRepository) {
    self.taskRepository = taskRepository
  }
  
  func execute(with task: Task, _ completion: (() -> Void)) {
    taskRepository.update(task: task, completion)
  }
}

// MARK: - Init

extension EditTask {
  public init() {
    self.taskRepository = InMemoryTaskRepository()
  }
}
