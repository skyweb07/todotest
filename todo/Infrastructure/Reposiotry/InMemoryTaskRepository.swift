import Foundation

final class InMemoryTaskRepository: TaskRepository {
  private var tasks = [Task]()
  
  func getAll(_ completion: ([Task]) -> Void) {
    completion(tasks)
  }
  
  func findWith(identifier: Int, _ completion: (Task?) -> Void) {
    let task = tasks.filter { $0.identifier == identifier }.first
    completion(task)
  }
  func add(task: Task, _ completion: () -> Void) {
    tasks.append(task)
    completion()
  }
  
  func update(task: Task, _ completion: () -> Void) {
    tasks = tasks.filter { $0.identifier != task.identifier }
    tasks.append(task)
    completion()
  }
  
  func delete(task: Task, _ completion: () -> Void) {
    tasks = tasks.filter { $0.identifier != task.identifier }
    completion()
  }
}
