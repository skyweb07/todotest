import Foundation

final class TaskPresenter {
  
  weak var view: TasksView?
  
  private let getAllTasks: GetAllTasks
  
  init(getAllTasks: GetAllTasks) {
    self.getAllTasks = getAllTasks
  }
  
  func didLoad() {
    getAllTasks.execute { [weak self] tasks in
      self?.view?.set(tasks: tasks)
    }
  }
}
