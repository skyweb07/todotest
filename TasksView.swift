import Foundation

protocol TasksView: class {
  func set(tasks: [Task])
}

