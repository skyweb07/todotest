import XCTest
@testable import todo

final class InMemoryTaskRepositorySpec: XCTestCase {
  
  private var sut: InMemoryTaskRepository!
  
  override func setUp() {
    sut = InMemoryTaskRepository()
  }
  
  override func tearDown() {
    sut = nil
  }
  
  func test_should_add_task() {
    let task = givenTask()
    
    let expectation = self.expectation(description: "add tasks")
    
    sut.add(task: task) {
      sut.findWith(identifier: task.identifier) { (result) in
        XCTAssertEqual(task.identifier, result?.identifier)
        expectation.fulfill()
      }
    }
    
    waitForExpectations(timeout: 1.0, handler: nil)
  }
  
  func test_should_delete_task() {
    let task = givenTask()
    
    let expectation = self.expectation(description: "add tasks")
    
    // This could be separated in different functions
    sut.add(task: task) {
      sut.delete(task: task) {
        sut.findWith(identifier: task.identifier) { (result) in
          XCTAssertNil(result)
          expectation.fulfill()
        }
      }
    }
    
    waitForExpectations(timeout: 1.0, handler: nil)
  }
  
  func test_should_find_task() {
    let task = givenTask()
    
    let expectation = self.expectation(description: "add tasks")
    
    // This could be separated in different functions
    sut.add(task: task) {
      sut.findWith(identifier: task.identifier) { (result) in
        XCTAssertNotNil(result)
        expectation.fulfill()
      }
    }
    
    waitForExpectations(timeout: 1.0, handler: nil)
  }
}

// MARK: - Helpers

extension InMemoryTaskRepositorySpec {
  
  fileprivate func givenTask() -> Task {
    Task(identifier: 1, name: "uno", dueDate: Date())
  }
  
  fileprivate func givenTasks() -> [Task] {
    return [
      Task(identifier: 1, name: "uno", dueDate: Date()),
      Task(identifier: 2, name: "dos", dueDate: Date()),
      Task(identifier: 3, name: "tre", dueDate: Date())
    ]
  }
}
